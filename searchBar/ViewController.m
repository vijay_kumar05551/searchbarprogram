//
//  ViewController.m
//  searchBar
//
//  Created by Click Labs135 on 10/6/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *friendsName;
NSMutableArray *searchResults;
@interface ViewController ()
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITableView *tableData;

@end

@implementation ViewController
@synthesize searchBar;
@synthesize tableData;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    friendsName=[[NSMutableArray alloc]init];
    searchResults=[[NSMutableArray alloc]init];
    friendsName=@[@"ujjwal",@"shreya",@"rahil",@"pooja",@"shiv",@"tamanna",@"garima",@"joshi",@"bunty",@"vicky",@"suchi",@"damini",@"akshay",@"vivek"];
    searchResults=[NSMutableArray arrayWithArray:friendsName];
    [tableData reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    cell.textLabel.text=friendsName[indexPath.row];
    cell.backgroundColor=[UIColor grayColor];
    return cell;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText

{
    
    NSPredicate *resultPredicate=[NSPredicate predicateWithFormat: @"SELF contains[c] %@",searchText];
    
    searchResults=[NSMutableArray arrayWithArray:[friendsName filteredArrayUsingPredicate:resultPredicate]];
    
    [tableData reloadData];
    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
