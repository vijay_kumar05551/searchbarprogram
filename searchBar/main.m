//
//  main.m
//  searchBar
//
//  Created by Click Labs135 on 10/6/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
